﻿using System;
using System.Collections.Generic;
using System.Linq;
using WpfApp1.Interfaces;
using WpfApp1.model;

namespace WpfApp1.ViewModels
{
    public class ReportDetailRepository : IReportDetailRepository
    {
        Model1 dataEntities = new Model1();
        public void AddReportDetail(ReportDetail reportDetail_)
        {
            using (var context = new Model1())
            {
                _ = context.ReportDetails.Add(
                       reportDetail_);
                context.SaveChanges();
            }
        }

        public void DeleteReportDetail(int segment)
        {
            using (var context = new Model1())
            {
                var bay = (from d in context.ReportDetails where d.Segment == segment select d).Single();
                context.ReportDetails.Remove(bay);
                context.SaveChanges();
            }
        }
        public void DeleteallItems()
        {
            using (var context = new Model1())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [ReportDetail]");
            }
        }
        public List<ReportDetail> GetReportDetailslist(int rid)

        {
            var query =
               (from Rd in dataEntities.ReportDetails
                where Rd.RId == rid
                select new { RDId = Rd.RDId, RId = Rd.RId, Segment = Rd.Segment, RType = Rd.RType, Stenosis = Rd.Stenosis, X = Rd.X, Y = Rd.Y, RLength = Rd.RLength })
              .ToList()
               .Select(a => new ReportDetail(a.RId, a.Segment, a.RType, a.Stenosis, a.X, a.Y, a.RLength)).ToList();
            return query;

        }

        public List<ReportDetail> UpdateReportDetail(ReportDetail reportDetail)
        {
            throw new NotImplementedException();
        }

        public void update_length(int segment, int? length)
        {
            using (var context = new Model1())
            {
                var result = context.ReportDetails.SingleOrDefault(b => b.Segment == segment);
                if (result != null)
                {
                    result.RLength = length;
                    context.SaveChanges();
                }
            }
        }
        public void update(int segment, byte rtype, byte stenosis)
        {
            using (var context = new Model1())
            {
                var result = context.ReportDetails.SingleOrDefault(b => b.Segment == segment);
                if (result != null)
                {
                    result.RType = rtype;
                    result.Stenosis = stenosis;
                    context.SaveChanges();
                }
            }
        }

    }
}
