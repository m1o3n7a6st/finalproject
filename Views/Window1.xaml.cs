﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.Enum_;

namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Stenosis Stensis { get; set; }
        public Window1( Stenosis stensis_)
        {
           
            InitializeComponent();
            Stensis = stensis_;
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           
            changeBackground((Button)e.Source);
            Stensis = Stenosis.low;
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            
            changeBackground((Button)e.Source);
            Stensis = Stenosis.medium;
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
          
            changeBackground((Button)e.Source);
            Stensis = Stenosis.high;
            this.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            
            changeBackground((Button)e.Source);
            Stensis = Stenosis.varyHigh;
            this.Close();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            
            changeBackground((Button)e.Source);
            Stensis = Stenosis.occlusin;
            this.Close();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
         
            changeBackground((Button)e.Source);
            Stensis = Stenosis.Undetermined;
            this.Close();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
           
            changeBackground((Button)e.Source);
            Stensis = Stenosis.stent;
            this.Close();
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            Stensis = Stenosis.unset;
            this.Close();
        }

        private void changeBackground( Button clicked)
        {
            if (Stensis != Stenosis.unset)
            {
                var name_=Enum.GetName(typeof(Stenosis), Stensis)+"btn";
                var btn =(Button)this.FindName(name_);
                btn.Background = Brushes.White;
            }
            clicked.Background = Brushes.LightSteelBlue;
        }
    }
}
