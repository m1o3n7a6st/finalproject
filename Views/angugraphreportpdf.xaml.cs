﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.ViewModels;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;

namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for angugraphreportpdf.xaml
    /// </summary>
    public partial class angugraphreportpdf : Window
    {
        ReportDetailRepository RDrep = new ReportDetailRepository();
#pragma warning disable CS0169 // The field 'angugraphreportpdf.text' is never used
        private string text;
#pragma warning restore CS0169 // The field 'angugraphreportpdf.text' is never used
#pragma warning disable CS0169 // The field 'angugraphreportpdf.itemsSource' is never used
        private IEnumerable itemsSource;
#pragma warning restore CS0169 // The field 'angugraphreportpdf.itemsSource' is never used

        public angugraphreportpdf( Canvas image , string conclution, IEnumerable itemsSource)
        {
            InitializeComponent();
            SetTime();
            this.ReportElements.ItemsSource = itemsSource;
            this.visual_images.Children.Add( XamlReader.Parse(XamlWriter.Save(image)) as Canvas);
            this.conclution.Text  ="          "+ conclution;

           
        }

    

        private static string UpdateDate(string date)
        {
            return date.PadLeft(2, '0');

        }
        private void SetTime()
        {
            DateTime date = DateTime.Now;
            PersianCalendar persian_date = new PersianCalendar();
            string year = Convert.ToString(persian_date.GetYear(date));
            string Month = Convert.ToString(persian_date.GetMonth(date));
            string day = Convert.ToString(persian_date.GetDayOfMonth(date));


           // this.time_.Text = year + "/" + UpdateDate(Month) + "/" + UpdateDate(day);
        }



    }
}
