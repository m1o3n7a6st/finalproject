﻿

using Microsoft.Win32;
using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using System.Xml;
using WpfApp1.tools;
namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for TAVI.xaml
    /// </summary>
    public partial class TAVI : Window
    {
        private SortedSet<int> listSelectedItems = new SortedSet<int>();
        private FlowDocument myFlowDocument = new FlowDocument();

        public TAVI()
        {
            InitializeComponent();
           // SetTime();
          //  this.WindowState = WindowState.Maximized;

        }
        private static string UpdateDate(string date)
        {
           return date.PadLeft(2, '0');

        }
        private void SetTime()
        {
            DateTime date = DateTime.Now;
            PersianCalendar persian_date = new PersianCalendar();
            string year = Convert.ToString(persian_date.GetYear(date));
            string Month = Convert.ToString(persian_date.GetMonth(date));
            string day = Convert.ToString(persian_date.GetDayOfMonth(date));


          //  this.time_.Text = year + "/" + UpdateDate(Month) + "/" + UpdateDate(day);
        }

        private void DoubleBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            var imgSrc = ((Button)sender).Tag;
            Tool.FilePicker(this.FindName(imgSrc.ToString()) as Image);
        }

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            var name = ((CheckBox)sender).Name;
            if (name.StartsWith("cheakBox"))

            {
                var str = name.Substring(name.Length - 1);
                ((Button)this.FindName("btn"+str)).IsEnabled = true;
                listSelectedItems.Add((int)char.GetNumericValue(name[name.Length - 1])); 
            }
        }

        private  void AddImage(System.Windows.Media.ImageSource src)
        {
            if (src == null)
                return;
            
            Image img = new Image();
            img.Height = 150;
            img.Width = 150;
            img.Visibility = Visibility.Visible;
            img.Source = src;
            image_report.Children.Add(img);

        }
        private void Page8_Leave(object sender, RoutedEventArgs e)
        {
            observations.Text = string.Empty;
            othersTxt.Text = string.Empty;
            concTxt.Text = string.Empty;
            image_report.Children.Clear();
            observations.Inlines.Add(new Bold(new Run("Observations:\n")));
            string space = "   ";
            foreach (int pageNumber in listSelectedItems)
            {
                switch (pageNumber)
                {
                    case 1:
                        observations.Inlines.Add(space + "1. Ascending Aorta with \n" + space + " Dimeter: " + input1.Value.ToString() + "\n");
                        AddImage(page1.Source);
                        break;
                    case 2:
                        observations.Inlines.Add(space + "2. Sino-tubular junction with  \n" + space + " Dimeter: " + input2.Value.ToString() + "\n");
                        AddImage(page2.Source);
                        break;
                    case 3:
                        observations.Inlines.Add(space + "3. Sinus of Valsalva with  \n " + space + " Dimeter1: " + input3_1.Value.ToString() + "\n  Dimeter2: " + input3_2.Value.ToString() + " \n  Dimeter3: " + input3_3.Value.ToString() + "\n");
                        AddImage(page3.Source);
                        break;
                    case 4:
                        observations.Inlines.Add(space + "4. Aortic valve annulus with \n " + space + " Width " + input4_1.Value.ToString() + " \n  Height: " + input4_2.Value.ToString() + " \n  Area: " + input4_3.Value.ToString() + "\n");
                        AddImage(page4.Source);
                        break;
                    case 5:
                        observations.Inlines.Add(space + "5. Left Main Ostium with \n" + space + " Distance: " + input5.Value.ToString() + "\n");
                        AddImage(page5.Source);
                        break;
                    case 6:
                        observations.Inlines.Add(space + "6. Right Coronary Ostium with \n" + space+ "Distance: " + input6.Value.ToString() + "\n");
                        AddImage(page6.Source);
                        break;
                    case 7:
                        observations.Inlines.Add(space + "7. Aortic value calcification and cups with  \n " + space + "Value Calcifications Score: " +
                           combBox7_1.Text + "\n " + space + "Number of Cusps:" + combBox7_2.Text + "\n");
                        AddImage(page7.Source);
                        break;
                    default:
                        break;
                }
            }
            if (others.Text.Length != 0)
            {
                othersTxt.IsEnabled = true;
                othersTxt.Inlines.Add(new Bold(new Run("Other findings:\n")));
                othersTxt.Inlines.Add(" " + others.Text + "\n");
            }
            if (conclution.Text.Length != 0)
            {
                concTxt.IsEnabled = true;
                concTxt.Inlines.Add(new Bold(new Run("Conclution:\n")));
                concTxt.Inlines.Add(" " + conclution.Text );
            }
        }

       
        private void Save_pdf(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Pdf Files|*.pdf";
            if (saveFileDialog.ShowDialog() == true)
            {
                string gridXaml = XamlWriter.Save(pdf);
                StringReader stringReader = new StringReader(gridXaml);
                XmlReader xmlReader = XmlReader.Create(stringReader);
                Grid newGrid = (Grid)XamlReader.Load(xmlReader);
                newGrid.Width = 768;
                newGrid.Height = 1104;
                XpsDocument xpsDocument = new XpsDocument("temp.xps", FileAccess.Write);
                XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(xpsDocument);
                writer.Write(newGrid);
                xpsDocument.Close();
                PdfDocument doc = new PdfDocument();
                doc.LoadFromFile("temp.xps", FileFormat.XPS);
                doc.SaveToFile(saveFileDialog.FileName, FileFormat.PDF);
            }
               
        }
    }
}