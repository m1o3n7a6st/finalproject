﻿using Microsoft.Win32;

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using PdfSharp;
using WpfApp1.Enum_;
using WpfApp1.model;
using WpfApp1.ViewModels;

using WpfApp1.Views;
using WpfApp1.Model;
using System.Linq;
using System.IO.Packaging;
using System.Threading.Tasks;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int Reportid = 1;
        public Items[] Stensis_ { get; set; }
        ReportDetailRepository RDrep = new ReportDetailRepository();

        public MainWindow()
        {
            InitializeComponent();
            Stensis_ = new Items[15];
            inish_stensis();
            RDrep.DeleteallItems();
            ReportElements.ItemsSource = RDrep.GetReportDetailslist(Reportid);
        }

        private void Reset_stensis()
        {
            for (int i = 0; i < 15; i++)
            {

                clean_segment(i + 1);
            }

        }

        private void clean_item(int segment, bool update = true)
        {
            Stensis_[segment - 1].canvasPlace = null;
            Stensis_[segment - 1].stenosis = Stenosis.unset;
            Stensis_[segment - 1].firsttime = true;
            if (update)
            {
                ReportElements.ItemsSource = RDrep.GetReportDetailslist(Reportid);
            }

        }

        private void clean_segment(int segment)
        {
            if (!Stensis_[segment - 1].firsttime)
            {
                visual_images.Children.Remove(Stensis_[segment - 1].canvasPlace);
            }
            clean_item(segment);
        }

        private void inish_stensis()
        {
            for (int i = 0; i < 15; i++)
            {
                Stensis_[i] = new Items();
            }

        }

        private void ResetClick(object sender, RoutedEventArgs e)
        {
            this.conclution.Text = "";
            this.DataContext = null;
            RDrep.DeleteallItems();
            Reset_stensis();
            ReportElements.ItemsSource = RDrep.GetReportDetailslist(Reportid);
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            var row = GetParent<DataGridRow>((Button)sender);
            RDrep.DeleteReportDetail(((ReportDetail)row.Item).Segment);
            clean_segment(Convert.ToInt16(((ReportDetail)row.Item).Segment));

        }

        private TargetType GetParent<TargetType>(DependencyObject o)
        where TargetType : DependencyObject
        {
            if (o == null || o is TargetType) return (TargetType)o;
            return GetParent<TargetType>(VisualTreeHelper.GetParent(o));
        }

        private void create_report(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Pdf Files|*.pdf";
            if (saveFileDialog.ShowDialog() == true)
            {
                angugraphreportpdf window1 = new angugraphreportpdf(visual_images,conclution.Text, ReportElements.ItemsSource);
                MemoryStream lMemoryStream = new MemoryStream();
                Package package = Package.Open(lMemoryStream, FileMode.Create);
                XpsDocument doc = new XpsDocument(package);
                XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(window1.report);
                doc.Close();
                package.Close();
                PdfSharp.Xps.XpsModel.XpsDocument pdfXpsDoc = PdfSharp.Xps.XpsModel.XpsDocument.Open(lMemoryStream); 
                PdfSharp.Xps.XpsConverter.Convert(pdfXpsDoc, saveFileDialog.FileName, 0);
                window1.Close();

            }
        }

        private void Path_MouseEnter(object sender, MouseEventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromArgb(255, 230, 206, 240);
            ((System.Windows.Shapes.Path)sender).Fill = mySolidColorBrush;
        }

        private void Path_MouseLeave(object sender, MouseEventArgs e)
        {
            ((System.Windows.Shapes.Path)sender).Fill = new SolidColorBrush();
        }

        private void Path_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Shapes.Path path_ = e.Source as System.Windows.Shapes.Path;

            int Segment = Int16.Parse(path_.Uid);
            Window1 window1 = new Window1(Stensis_[Segment - 1].stenosis);
            window1.ShowDialog();
            if (Stensis_[Segment - 1].stenosis != window1.Stensis)
            {
                Stensis_[Segment - 1].stenosis = window1.Stensis;
                if (!Stensis_[Segment - 1].firsttime)
                {
                    visual_images.Children.Remove(Stensis_[Segment - 1].canvasPlace);
                    RDrep.DeleteReportDetail(Stensis_[Segment - 1].reportDetail.Segment);
                }
                if (Stensis_[Segment - 1].stenosis == Stenosis.unset)
                {
                    clean_item(Segment, true);
                }

                draw_image(Segment, path_);
                Stensis_[Segment - 1].reportDetail = new ReportDetail(Reportid, (byte)Segment, (byte)MyType.Mixed, (byte)Stensis_[Segment - 1].stenosis, 0, 0, 0);
                RDrep.AddReportDetail(Stensis_[Segment - 1].reportDetail);
                ReportElements.ItemsSource = RDrep.GetReportDetailslist(Reportid);
            }
        }

        private void draw_image(int Segment, System.Windows.Shapes.Path path_)
        {
            string BodyName = "image" + Enum.GetName(typeof(Stenosis), Stensis_[Segment - 1].stenosis) + Segment;
            Image BodyImage = new Image
            {
                Width = path_.ActualWidth > 60 ? 60 : path_.ActualWidth,
                Height = path_.ActualHeight > 60 ? 60 : path_.ActualHeight,
                Name = BodyName,
                Source = new BitmapImage(
                    new Uri(
                        "../../static/image/circle/" +
                        Enum.GetName(typeof(Stenosis), Stensis_[Segment - 1].stenosis) +
                        ".png",
                    UriKind.Relative)
                    ),
                IsHitTestVisible = false
            };
             
            double top = Canvas.GetTop(path_);
            double left = Canvas.GetLeft(path_);
            Stensis_[Segment - 1].canvasPlace = BodyImage;
            visual_images.Children.Add(BodyImage);
            Stensis_[Segment - 1].firsttime = false;
            
            Canvas.SetTop(BodyImage, path_.ActualHeight > 60 ? top+(path_.ActualHeight-60)/2 : top);
            Canvas.SetLeft(BodyImage, path_.ActualWidth > 60 ? left+(path_.ActualWidth-60)/2 :left);
            Canvas.SetZIndex(BodyImage, 1);
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            var row = GetParent<DataGridRow>((ComboBox)sender);
            int segment = ((ReportDetail)row.Item).Segment;
            visual_images.Children.Remove(Stensis_[segment - 1].canvasPlace);
            Stensis_[segment - 1].stenosis = ((ReportDetail)row.Item).RStenosis;
            RDrep.update(segment, (byte)((ReportDetail)row.Item).Type.Value, (byte)((ReportDetail)row.Item).RStenosis);
            ReportElements.ItemsSource = RDrep.GetReportDetailslist(Reportid);
            draw_image(segment, (System.Windows.Shapes.Path)GetByUid(visual_images, segment.ToString()));


        }

        public static UIElement GetByUid(DependencyObject rootElement, string uid)
        {
            foreach (UIElement element in LogicalTreeHelper.GetChildren(rootElement).OfType<UIElement>())
            {
                if (element.Uid == uid)
                    return element;
                UIElement resultChildren = GetByUid(element, uid);
                if (resultChildren != null)
                    return resultChildren;
            }
            return null;
        }

        private void pdf()
        {
            return;
        }

        private void ReportElements_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.Column.Header.ToString() == "Length")
            {
                TextBox t = e.EditingElement as TextBox;
                ReportDetail changed = ((ReportDetail)e.Row.Item);
                RDrep.update_length(changed.Segment, Convert.ToInt16( t.Text));
                ReportElements.ItemsSource = RDrep.GetReportDetailslist(Reportid);
            }
        }

        private void Path_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromArgb(255, 230, 206, 240);
            ((System.Windows.Shapes.Path)sender).Fill = mySolidColorBrush;
        }
    }
}
