﻿namespace WpfApp1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientReport",
                c => new
                    {
                        PRId = c.Int(nullable: false, identity: true),
                        ReportId = c.Int(),
                        PatientId = c.Int(),
                    })
                .PrimaryKey(t => t.PRId)
                .ForeignKey("dbo.Patient", t => t.PatientId, cascadeDelete: true)
                .ForeignKey("dbo.Report", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Patient",
                c => new
                    {
                        PatientId = c.Int(nullable: false, identity: true),
                        PFName = c.String(maxLength: 255),
                        PLName = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.PatientId);
            
            CreateTable(
                "dbo.Report",
                c => new
                    {
                        ReportId = c.Int(nullable: false, identity: true),
                        Conclusion = c.String(),
                    })
                .PrimaryKey(t => t.ReportId);
            
            CreateTable(
                "dbo.ReportDetail",
                c => new
                    {
                        RDId = c.Int(nullable: false, identity: true),
                        RId = c.Int(),
                        Segment = c.Byte(nullable: false),
                        RType = c.Byte(nullable: false),
                        Stenosis = c.Byte(nullable: false),
                        RLength = c.Int(),
                        X = c.Int(),
                        Y = c.Int(),
                        Type = c.Byte(),
                        RStenosis = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.RDId)
                .ForeignKey("dbo.Report", t => t.RId, cascadeDelete: true)
                .Index(t => t.RId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReportDetail", "RId", "dbo.Report");
            DropForeignKey("dbo.PatientReport", "ReportId", "dbo.Report");
            DropForeignKey("dbo.PatientReport", "PatientId", "dbo.Patient");
            DropIndex("dbo.ReportDetail", new[] { "RId" });
            DropIndex("dbo.PatientReport", new[] { "PatientId" });
            DropIndex("dbo.PatientReport", new[] { "ReportId" });
            DropTable("dbo.ReportDetail");
            DropTable("dbo.Report");
            DropTable("dbo.Patient");
            DropTable("dbo.PatientReport");
        }
    }
}
