namespace WpfApp1.model
{
    using System.Data.Entity;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=WpfApp1.Properties.Settings.FinalProjectConnectionString")
        {
            Database.SetInitializer(new ReportDBInitializer());
        }

        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<PatientReport> PatientReports { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<ReportDetail> ReportDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Patient>()
                .HasMany(e => e.PatientReports)
                .WithOptional(e => e.Patient)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Report>()
                .HasMany(e => e.PatientReports)
                .WithOptional(e => e.Report)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Report>()
                .HasMany(e => e.ReportDetails)
                .WithOptional(e => e.Report)
                .HasForeignKey(e => e.RId)
                .WillCascadeOnDelete();
        }
    }
    public class ReportDBInitializer : CreateDatabaseIfNotExists<Model1>
    {
        protected override void Seed(Model1 context)
        {
            context.Patients.Add(new Patient() { PFName = "user", PLName = "user" });
            context.SaveChanges();
            context.Reports.Add(new Report() { Conclusion = " " });
            context.SaveChanges();
            context.PatientReports.Add(new PatientReport() { PatientId = 1, ReportId = 1 });
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
