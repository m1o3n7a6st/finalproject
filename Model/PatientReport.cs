namespace WpfApp1.model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PatientReport")]
    public partial class PatientReport
    {
        [Key]
        public int PRId { get; set; }

        public int? ReportId { get; set; }

        public int? PatientId { get; set; }

        public virtual Patient Patient { get; set; }

        public virtual Report Report { get; set; }
    }
}
