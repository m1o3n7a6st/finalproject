namespace WpfApp1.model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using WpfApp1.Enum_;

    [Table("ReportDetail")]
    public partial class ReportDetail
    {
        public ReportDetail()
        {
        }

        public ReportDetail(int? rId, byte segment, byte rType, byte stenosis, int? x, int? y, int? rLength = 0)
        {
            RId = rId;
            Segment = segment;
            RType = rType;
            Stenosis = stenosis;
            X = x;
            Y = y;
            RLength = rLength;
            Type = (MyType)rType;
            RStenosis = (Stenosis)stenosis;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RDId { get; set; }

        public int? RId { get; set; }

        public byte Segment { get; set; }

        public byte RType { get; set; }

        public byte Stenosis { get; set; }

        public int? RLength { get; set; }

        public int? X { get; set; }

        public int? Y { get; set; }

        public virtual Report Report { get; set; }
        public MyType? Type { get; set; }
        public Stenosis RStenosis { get; set; }

        public MyType GetRtype()
        {
            return (MyType)RType;
        }
        public string GetStenosis()
        {
            return Enum.GetName(typeof(Stenosis), Convert.ToInt32(Stenosis));
        }
    }
}
