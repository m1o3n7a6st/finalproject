﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfApp1.Enum_;
using WpfApp1.model;

namespace WpfApp1.Model
{
    public class Items
    {
        public Items()
        {
            this.stenosis = Stenosis.unset;
            this.canvasPlace = null;
            this.firsttime = true;

        }

        public Stenosis stenosis { get; set; }
        public UIElement canvasPlace { get; set; }
        public bool firsttime { get; set; }
        public ReportDetail reportDetail{ get; set; }

    }
}
