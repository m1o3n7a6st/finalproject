﻿using System.Collections.Generic;
using WpfApp1.model;

namespace WpfApp1.Interfaces
{
    public interface IReportDetailRepository
    {
        List<ReportDetail> GetReportDetailslist(int rid);
        void AddReportDetail(ReportDetail reportDetail);
        void DeleteReportDetail(int segment);
        List<ReportDetail> UpdateReportDetail(ReportDetail reportDetail);
    }
}
